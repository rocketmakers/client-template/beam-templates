# rivar-templates

Repository for managing notification templates with git and `@rocketmakers/orbit-template-http-repository`

# [Layouts](./docs/layouts.md)

# [Partials](./docs/partials.md)

# [`<provider>.json`](./docs/providerJson.md)

# [Managing templates](./docs/managingTemplates.md)

# Releases

Different environments of Beam point at different versions of the templates. This is to ensure if payloads change, that other environments are not effected. These environments are as follows.

- `develop` - local and https://test.beam3d.app
- `demo` - https://demo.beam3d.app
