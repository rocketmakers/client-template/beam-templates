/**
 * Specify required object
 *
 * @examples require(".").sampleData
 */
export interface IModel {
  username: string;
  callbackUrl: string;
  verifyUserToken: string;
}

export const sampleData: IModel[] = [
  {
    username: "david@rocketmakers.com",
    verifyUserToken: "TEST_TOKEN",
    callbackUrl: "https://beam3d/app",
  },
];
