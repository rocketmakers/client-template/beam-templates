/**
 * Specify required object
 *
 * @examples require(".").sampleData
 */
export interface IModel {
  username: string;
}

export const sampleData: IModel[] = [
  {
    username: "test@rocketmakers.com",
  },
];
